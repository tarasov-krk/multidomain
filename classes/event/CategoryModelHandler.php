<?php namespace Itcom\Multidomain\Classes\Event;

use Event;
use Itcom\Multidomain\Classes\DomainService;
use Itcom\Multidomain\Classes\ExtendsCategoryService;
use Itcom\Multidomain\Models\Domain;
use Itcom\Multidomain\Models\ExtendCategory;
use Lovata\Toolbox\Classes\Event\ModelHandler;

use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Classes\Item\CategoryItem;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;

use Lovata\Shopaholic\Controllers\Categories;

/**
 * Class ProductModelHandler
 */
class CategoryModelHandler extends ModelHandler
{
    /** @var  Category */
    protected $obElement;

    /**
     * Add listeners
     *
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {
        parent::subscribe($obEvent);

//        Category::extend(function ($obElement)
//        {
//            $this->extendCategoryModel($obElement);
//        });

        CategoryItem::extend(function ($obItem)
        {
            $this->extendCategoryItem($obItem);
        });
    }

    /**
     * After save event handler
     */
    protected function afterSave()
    {
    }

    /**
     * After delete event handler
     */
    protected function afterDelete()
    {
    }

    /**
     * Extend category model
     *
     * @param Category $obElement
     */
    protected function extendCategoryModel($obElement)
    {
        $obElement->hasOne['multidomain_category'] = [
            ExtendCategory::class,
        ];

        $obElement->bindEvent('model.beforeDelete', function () use ($obElement)
        {
            if ($obElement->multidomain_category) {
                $obElement->multidomain_category->delete();
            }
        });
    }

    /**
     * Extend product item
     *
     * @param CategoryItem $obItem
     */
    protected function extendCategoryItem($obItem)
    {
        $obItem->addDynamicMethod('getDescription', function () use ($obItem)
        {
            $result = $obItem->description;

            $obExtendsCategory = null;
            if ($obCurrentDomain = DomainService::getCurrentDomainData()) {
                $obExtendsCategory = ExtendsCategoryService::getExtendsCategoryData($obItem->id, $obCurrentDomain->id);
            }

            if ($obExtendsCategory && !empty($obExtendsCategory->category_description)) {
                $result = $obExtendsCategory->category_description;
            }

            return $result;
        });

        $obItem->addDynamicMethod('getPreviewText', function () use ($obItem)
        {
            $result = $obItem->preview_text;

            $obExtendsCategory = null;
            if ($obCurrentDomain = DomainService::getCurrentDomainData()) {
                $obExtendsCategory = ExtendsCategoryService::getExtendsCategoryData($obItem->id, $obCurrentDomain->id);
            }

            if ($obExtendsCategory && !empty($obExtendsCategory->category_preview_text)) {
                $result = $obExtendsCategory->category_preview_text;
            }

            return $result;
        });
    }

    /**
     * Get model class name
     *
     * @return string
     */
    protected function getModelClass()
    {
        return Category::class;
    }

    /**
     * Get item class name
     *
     * @return string
     */
    protected function getItemClass()
    {
        return CategoryItem::class;
    }
}
