<?php namespace Itcom\Multidomain\Classes\Event;

use Itcom\Multidomain\Classes\ExtendsCategoryService;
use Itcom\Multidomain\Models\ExtendCategory;
use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Controllers\Categories;

/**
 * Class CategoryControllerHandler
 */
class CategoryControllerHandler
{
    private $extendsFieldsCategories = '/itcom/multidomain/models/extendcategory/fields.yaml';

    /**
     * Add listeners
     *
     * @param \Illuminate\Events\Dispatcher $obEvent
     */
    public function subscribe($obEvent)
    {
        $obEvent->listen('backend.form.extendFields', function ($obWidget)
        {
            $this->extendFields($obWidget);
            $this->extendMethods($obWidget);
        });
    }

    /**
     * Extend categories controller
     *
     * @param Categories $obController
     */
    protected function extendMethods($obController)
    {
        $obController->addDynamicMethod('onLoadExtendsCategoryContent', function ($categoryId) use ($obController)
        {
            $domainId = input('domain_id');
            $domainName = input('domain_name');

            if (!$categoryId || !$domainId) {
                throw new \ApplicationException('Неверные параметры');
            }

            $data['city'] = $domainName;
            $data['preview_text'] = '';
            $data['description'] = '';
            $data['domain_id'] = $domainId;

            /**
             * @var ExtendCategory $obExtendsCategory
             */
            if ($obExtendsCategory = ExtendsCategoryService::getExtendsCategoryData($categoryId, $domainId)) {
                $data['preview_text'] = $obExtendsCategory->category_preview_text;
                $data['description'] = $obExtendsCategory->category_description;
            }

            return $obController->makePartial('$/itcom/multidomain/view/category_item.htm', $data);
        });

        $obController->addDynamicMethod('onSaveExtendsCategory', function ($categoryId) use ($obController)
        {
            $domainId = input('domain_id');
            $extendsCategoryData = input('extendsCategory');

            ExtendsCategoryService::saveExtendsCategoryData($categoryId, $domainId,
                $extendsCategoryData['preview_text'], $extendsCategoryData['description']);

            return true;
        });
    }

    /**
     * Extend products field
     *
     * @param \Backend\Widgets\Form $obWidget
     */
    protected function extendFields($obWidget)
    {
        if (!$obWidget->getController() instanceof Categories || $obWidget->isNested) {
            return;
        }

        if (!$obWidget->model instanceof Category) {
            return;
        }

        if (!file_exists(plugins_path($this->extendsFieldsCategories))) {
            return;
        }

        $arAdditionFields = \Yaml::parse(file_get_contents(plugins_path($this->extendsFieldsCategories)));

        $obWidget->addTabFields($arAdditionFields);
    }
}
