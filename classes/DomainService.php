<?php namespace Itcom\Multidomain\Classes;

use Request;
use Itcom\Multidomain\Models\Domain;

/**
 * Class DomainService
 *
 * @author a.tarasov
 * @package Itcom\Multidomain\Classes
 */
class DomainService
{
    /**
     * @var array Кэшированные данные по домену
     */
    private static $cacheCurrentDomainData = [];

    /**
     * @var array Кэшированные данные по списку доменов
     */
    private static $cacheListDomainData = [];

    /**
     * Получить данные по текущему поддомену
     *
     * @author a.tarasov
     * @access public
     * @return object Domain
     */
    public static function getCurrentDomainData()
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        if (!self::$cacheCurrentDomainData) {
            if ($host = Request::getHttpHost()) {
                self::$cacheCurrentDomainData = Domain::where('url', $host)->where('active', 1)->first();
            }
        }

        return self::$cacheCurrentDomainData;

        // </editor-fold>
    }

    /**
     * Получить данные по списку доменов
     *
     * @author a.tarasov
     * @access public
     * @return array
     */
    public static function getListDomains()
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        if (!self::$cacheListDomainData) {
            self::$cacheListDomainData = Domain::where('active', 1)->orderBy('position')->get();
        }

        return self::$cacheListDomainData;

        // </editor-fold>
    }
}