<?php namespace Itcom\Multidomain\Classes;

use DB;
use Lovata\Shopaholic\Models\Category;
use Request;
use Itcom\Multidomain\Models\Domain;
use Itcom\Multidomain\Models\Token;

/**
 * Class TokenService
 *
 * @author a.tarasov
 * @package Itcom\Multidomain\Classes
 */
class TokenService
{
    /**
     * @var array Кэшированные данные по токенам
     */
    private static $cacheTokensData = [];

    /**
     * @var string
     */
    const TOKEN_NAMESPASE = 'domainToken';

    /**
     * @var array Уникальные токены
     */
    const SPECIFIC_TOKENS = [
        'product_name'                       => [
            'model'     => 'Lovata\Shopaholic\Models\Product',
            'attribute' => 'name',
        ],
        'product_name_lower'                 => [
            'model'     => 'Lovata\Shopaholic\Models\Product',
            'attribute' => 'name',
            'lower'     => true,
        ],
        'product_price'                      => [
            'model'     => 'Lovata\Shopaholic\Models\Product',
            'attribute' => 'price_value',
            'offer'     => true,
        ],
        'product_price_old'                  => [
            'model'     => 'Lovata\Shopaholic\Models\Product',
            'attribute' => 'old_price_value',
            'offer'     => true,
        ],
        'product_category_name'              => [
            'model'            => 'Lovata\Shopaholic\Models\Product',
            'attribute'        => 'name',
            'product_category' => true,
        ],
        'product_category_name_lower'        => [
            'model'            => 'Lovata\Shopaholic\Models\Product',
            'attribute'        => 'name',
            'product_category' => true,
            'lower'            => true,
        ],
        'product_category_parent_name'       => [
            'model'            => 'Lovata\Shopaholic\Models\Product',
            'attribute'        => 'name',
            'product_category' => true,
            'parent'           => true,
        ],
        'product_category_parent_name_lower' => [
            'model'            => 'Lovata\Shopaholic\Models\Product',
            'attribute'        => 'name',
            'product_category' => true,
            'parent'           => true,
            'lower'            => true,
        ],
        'product_price_min'                  => [
            'model'             => 'Lovata\Shopaholic\Models\Product',
            'attribute'         => 'name',
            'product_category'  => true,
            'min_price_product' => true,
        ],
        'category_name'                      => [
            'model'     => 'Lovata\Shopaholic\Models\Category',
            'attribute' => 'name',
        ],
        'category_name_lower'                => [
            'model'     => 'Lovata\Shopaholic\Models\Category',
            'attribute' => 'name',
            'lower'     => true,
        ],
        'category_parent_name'               => [
            'model'     => 'Lovata\Shopaholic\Models\Category',
            'attribute' => 'name',
            'parent'    => true,
        ],
        'category_parent_name_lower'         => [
            'model'     => 'Lovata\Shopaholic\Models\Category',
            'attribute' => 'name',
            'lower'     => true,
            'parent'    => true,
        ],
        'category_price_min'                 => [
            'model'             => 'Lovata\Shopaholic\Models\Category',
            'attribute'         => 'name',
            'min_price_product' => true,
        ],
    ];

    /**
     * Получить все токены для текущего домена
     *
     * @param object $domain
     * @return array
     */
    public static function getTokensByDomain($domain)
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        if (!$domain instanceof Domain || empty($domain->id)) {
            return [];
        }

        if (!self::$cacheTokensData) {

            if ($domainAttributes = $domain->getAttributes()) {
                foreach ($domainAttributes as $name => $value) {
                    self::$cacheTokensData[$name] = $value;
                }
            }

            if ($tokensList = Token::where('domain_id', $domain->id)->orWhere('domain_id', null)->get()) {
                foreach ($tokensList as $token) {
                    /** @var Token $token */
                    self::$cacheTokensData[$token->name] = $token->value;
                }
            }
        }

        return self::$cacheTokensData;

        // </editor-fold>
    }

    /**
     * @param string $token
     * @param array  $cacheTokens
     * @return array
     */
    protected static function loadSpecificTokens($token, &$cacheTokens)
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        if (array_key_exists($token, self::SPECIFIC_TOKENS)) {
            $model = self::SPECIFIC_TOKENS[$token]['model'];
            $attribute = self::SPECIFIC_TOKENS[$token]['attribute'];
            $lower = empty(self::SPECIFIC_TOKENS[$token]['lower']) ? false : true;
            $parent = empty(self::SPECIFIC_TOKENS[$token]['parent']) ? false : true;
            $offer = empty(self::SPECIFIC_TOKENS[$token]['offer']) ? false : true;
            $product_category = empty(self::SPECIFIC_TOKENS[$token]['product_category']) ? false : true;
            $min_price_product = empty(self::SPECIFIC_TOKENS[$token]['min_price_product']) ? false : true;

            if (!$slug = self::getCurrentSlug()) {
                return $cacheTokens;
            }

            if (!$obModel = $model::where('slug', $slug)->first()) {
                return $cacheTokens;
            }

            // Найти цены продукта
            if ($offer && !empty($obModel->offer)) {
                $obModel = $obModel->offer->first();
            }

            // Найти категорию продукта
            if ($product_category && !empty($obModel->category)) {
                $obModel = $obModel->category->first();
            }

            // Найти родительскую категорию
            if ($parent && !empty($obModel->parent_id)) {
                $obModel = $model::find($obModel->parent_id);
            }

            if (empty($obModel->{$attribute})) {
                return $cacheTokens;
            }

            $tokenValue = $obModel->{$attribute};

            // Найти минимальную цену товара в категории
            if ($min_price_product && $obModel instanceof Category) {
                $data = DB::select("
                SELECT
                 of.price
                FROM 
                 lovata_shopaholic_products AS prod
                LEFT JOIN
                 lovata_shopaholic_offers AS of ON of.product_id = prod.id
                LEFT JOIN
                 lovata_shopaholic_additional_categories AS adC ON adC.product_id = prod.id
                WHERE
                 (prod.category_id = :id OR adC.category_id = :id2) AND of.price > 0
                ORDER BY
                 of.price
                LIMIT 1
                ", ['id' => $obModel->id, 'id2' => $obModel->id]);

                $data = reset($data);
                if (!empty($data->price)) {
                    $tokenValue = number_format($data->price, 0, '', ' ');
                }
            }

            // формат цены, без копеек
            if (in_array($attribute, ['price_value', 'old_price_value']) && $tokenValue > 0){
                $tokenValue = number_format($tokenValue, 0, '', ' ');
            }

            // Преобразовать в нижний регистр
            if ($lower) {
                $tokenValue = mb_strtolower($tokenValue);
            }

            $cacheTokens = array_merge($cacheTokens, [
                $token => $tokenValue,
            ]);

            self::$cacheTokensData = array_merge(self::$cacheTokensData, [
                $token => $tokenValue,
            ]);
        }

        return $cacheTokens;

        // </editor-fold>
    }

    /**
     * Get current slug
     *
     * @author a.tarasov
     * @return string
     */
    protected static function getCurrentSlug()
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        $result = '';

        if ($uri = Request::getRequestUri()) {
            $uriEx = explode("/", $uri);
            $result = end($uriEx);
        }

        return $result;

        // </editor-fold>
    }

    /**
     * @author a.tarasov
     * @access public
     * @param object $domain
     * @return array
     */
    public static function getTokensVarsForController($domain)
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        if (!$domain instanceof Domain || empty($domain->id)) {
            return [];
        }

        if (!$tokens = self::getTokensByDomain($domain)) {
            return [];
        }

        return [
            self::TOKEN_NAMESPASE => $tokens,
        ];

        // </editor-fold>
    }

    /**
     * Заменить все значения токенов в указанной строке
     *
     * @param string $content
     * @param object $domain
     * @return string|string[]|null
     */
    public static function replaceTokensContent($content, $domain)
    {
        if (!$domain instanceof Domain || empty($domain->id)) {
            return $content;
        }

        $pattern = "/{{ " . self::TOKEN_NAMESPASE . ".(.[^\s]*) }}/";

        if (preg_match_all($pattern, $content, $matches, PREG_PATTERN_ORDER)) {
            if (!empty($matches[1])) {

                if (!$tokens = self::getTokensByDomain($domain)) {
                    return $content;
                }

                foreach ($matches[1] as $matchKey => $match) {
                    $replacement = '';
                    if (array_key_exists($match, $tokens)) {
                        $replacement = $tokens[$match];
                    }
                    else {
                        // Попытка найти уникальный токен
                        self::loadSpecificTokens($match, $tokens);
                        if (array_key_exists($match, $tokens)) {
                            $replacement = $tokens[$match];
                        }
                    }

                    $content = str_replace($matches[0][$matchKey], $replacement, $content);
                }
            }
        }

        return $content;
    }

}