<?php namespace Itcom\Multidomain\Classes;

use Request;
use Itcom\Multidomain\Models\ExtendCategory;

/**
 * Class ExtendsCategoryService
 *
 * @author a.tarasov
 * @package Itcom\Multidomain\Classes
 */
class ExtendsCategoryService
{
    /**
     * @var array Кэшированные данные
     */
    private static $cacheExtendsCategoryData = [];

    /**
     * Получить данные по категориям
     *
     * @author a.tarasov
     * @access public
     * @param int $categoryId
     * @param int $domainId
     * @return object ExtendCategory
     */
    public static function getExtendsCategoryData($categoryId, $domainId)
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        if (!self::$cacheExtendsCategoryData) {
            self::$cacheExtendsCategoryData = ExtendCategory::where('category_id', $categoryId)
                ->where('domain_id', $domainId)
                ->first();
        }

        return self::$cacheExtendsCategoryData;

        // </editor-fold>
    }

    /**
     * Сохранить описание категорий для домена
     *
     * @author a.tarasov
     * @access public
     * @param int $categoryId
     * @param int $domainId
     * @param string $preview_text
     * @param string $description
     * @return void
     */
    public static function saveExtendsCategoryData($categoryId, $domainId, $preview_text = null, $description = null)
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        /**
         * @var ExtendCategory $obExtendCategory
         */
        $obExtendCategory = self::getExtendsCategoryData($categoryId, $domainId);

        if (!$obExtendCategory) {
            $obExtendCategory = new ExtendCategory();
            $obExtendCategory->category_id = $categoryId;
            $obExtendCategory->domain_id = $domainId;
        }

        $obExtendCategory->category_preview_text = $preview_text;
        $obExtendCategory->category_description = $description;
        $obExtendCategory->save();

        // </editor-fold>
    }
}