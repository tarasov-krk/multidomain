<?php namespace Itcom\Multidomain\Models;

use Model;

/**
 * Domain Model
 *
 * @property integer $id
 * @property string  $name
 * @property string  $url
 * @property string  $phone1
 * @property string  $phone2
 * @property string  $address
 * @property string  $map
 *
 */
class Domain extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string Table name
     */
    const TABLE = 'itcom_multidomain_domains';

    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = self::TABLE;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $rules = [
        'name' => 'required',
        'url'  => 'required',
    ];

}
