<?php namespace Itcom\Multidomain\Models;

use Lovata\Shopaholic\Models\Category;
use Model;
use Itcom\Multidomain\Models\Domain;

/**
 * Model
 *
 * @property Category $category;
 * @property Domain   $domain;
 * @property string   $category_preview_text;
 * @property string   $category_description;
 *
 */
class ExtendCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string Table name
     */
    const TABLE = 'itcom_multidomain_extend_category';

    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = self::TABLE;

    /**
     * @var array Validation rules
     */
    public $rules = [];

    public $belongsTo = [
        'domain' => Domain::class,
    ];
}
