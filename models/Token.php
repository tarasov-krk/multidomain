<?php namespace Itcom\Multidomain\Models;

use Model;
use Itcom\Multidomain\Models\Domain;

/**
 * Model
 *
 * @property string $name;
 * @property string $value;
 * @property Domain $domain;
 *
 */
class Token extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string Table name
     */
    const TABLE = 'itcom_multidomain_tokens';

    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = self::TABLE;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'  => 'required',
        'value' => 'required',
    ];

    public $belongsTo = [
        'domain' => Domain::class,
    ];
}
