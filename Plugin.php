<?php namespace Itcom\Multidomain;

use Event;
use Backend;
use Cms\Classes\Controller as CmsController;
use Itcom\Multidomain\Classes\DomainService;
use Itcom\Multidomain\Classes\Event\CategoryControllerHandler;
use Itcom\Multidomain\Classes\Event\CategoryModelHandler;
use Itcom\Multidomain\Classes\TokenService;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    /**
     * Plugin boot method
     */
    public function boot()
    {
        Event::subscribe(CategoryControllerHandler::class);
        Event::subscribe(CategoryModelHandler::class);
    }

    public function registerComponents()
    {
        return [
            '\Itcom\Multidomain\Components\CitySelect' => 'CitySelect',
        ];
    }

    public function registerSettings()
    {
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'itcom.multidomain.setting_multidomains' => [
                'tab'   => 'itcom.multidomain::lang.plugin.name',
                'label' => 'itcom.multidomain::lang.common.setting_multidomains',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'multidomain' => [
                'label'       => 'itcom.multidomain::lang.plugin.name',
                'url'         => Backend::url('itcom/multidomain/domains'),
                'icon'        => 'oc-icon-cubes',
                'permissions' => ['itcom.multidomain.setting_multidomains'],
                'order'       => 500,
                'sideMenu'    => [
                    'domains' => [
                        'label'       => 'itcom.multidomain::lang.common.domains',
                        'icon'        => 'icon-globe',
                        'permissions' => ['itcom.multidomain.setting_multidomains'],
                        'url'         => Backend::url('itcom/multidomain/domains'),
                    ],
                    'tokens'  => [
                        'label'       => 'itcom.multidomain::lang.common.tokens',
                        'icon'        => 'icon-retweet',
                        'permissions' => ['itcom.multidomain.setting_multidomains'],
                        'url'         => Backend::url('itcom/multidomain/tokens'),
                    ],
                    'info'    => [
                        'label'       => 'itcom.multidomain::lang.common.info',
                        'icon'        => 'icon-info-circle',
                        'permissions' => ['itcom.multidomain.setting_multidomains'],
                        'url'         => Backend::url('itcom/multidomain/info'),
                    ],
                ],
            ],
        ];
    }

    /**
     * Register our event listener where we inject the variables
     * and our main classes
     */
    public function register()
    {
        /**
         * Register a listener to inject the tokens before the
         * page and layout are rendered
         */
        Event::listen('cms.page.init', function (CmsController $controller = null)
        {
            if (null === $controller) {
                return;
            }

            $domain = DomainService::getCurrentDomainData();

            if ($tokens = TokenService::getTokensVarsForController($domain)) {
                $controller->vars = array_merge($controller->vars, $tokens);
            }
        });

        Event::listen('seo.beforeComponentRender', function ($page, $seoTag)
        {
            if (null === $seoTag) {
                return;
            }

            $domain = DomainService::getCurrentDomainData();

            $seoTag->meta_title = TokenService::replaceTokensContent($seoTag->meta_title, $domain);
            $seoTag->meta_description = TokenService::replaceTokensContent($seoTag->meta_description, $domain);
            $seoTag->meta_keywords = TokenService::replaceTokensContent($seoTag->meta_keywords, $domain);
            $seoTag->og_title = TokenService::replaceTokensContent($seoTag->og_title, $domain);
            $seoTag->og_description = TokenService::replaceTokensContent($seoTag->og_description, $domain);
        });

    }

    /**
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'token_filter' => [$this, 'tokenFilter'],
            ],
        ];
    }

    public function tokenFilter($text)
    {
        $domain = DomainService::getCurrentDomainData();
        $text = TokenService::replaceTokensContent($text, $domain);

        return $text;
    }
}
