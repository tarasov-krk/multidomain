<?php namespace Itcom\Servicepage\Updates;

use Itcom\Multidomain\Models\Domain;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddMetatagsToDomain extends Migration
{
    public function up()
    {
        Schema::table(Domain::TABLE, function (Blueprint $table)
        {
            $table->text('metatags')->nullable();
        });
    }

    public function down()
    {
        Schema::table(Domain::TABLE, function (Blueprint $table)
        {
            $table->dropColumn('metatags');
        });
    }
}
