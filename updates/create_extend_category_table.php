<?php namespace Itcom\Multidomain\Updates;

use Itcom\Multidomain\Models\ExtendCategory;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExtendCategoryTable extends Migration
{
    public function up()
    {
        Schema::create(ExtendCategory::TABLE, function(Blueprint $table) {
            Schema::dropIfExists(ExtendCategory::TABLE);
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('domain_id');
            $table->text('category_preview_text')->nullable();
            $table->text('category_description')->nullable();
            $table->index(['category_id', 'domain_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists(ExtendCategory::TABLE);
    }
}