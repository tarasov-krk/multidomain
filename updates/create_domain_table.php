<?php namespace Itcom\Servicepage\Updates;

use Itcom\Multidomain\Models\Domain;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDomainTable extends Migration
{
    public function up()
    {
        Schema::create(Domain::TABLE, function(Blueprint $table) {
            Schema::dropIfExists(Domain::TABLE);
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('is_main')->default(0);
            $table->string('name', 50);
            $table->string('url', 125);
            $table->string('phone1', 50)->nullable();
            $table->string('phone2', 50)->nullable();
            $table->string('address', 255)->nullable();
            $table->text('map')->nullable();
            $table->tinyInteger('position')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists(Domain::TABLE);
    }
}
