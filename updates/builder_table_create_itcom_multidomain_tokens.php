<?php namespace Itcom\Multidomain\Updates;

use Itcom\Multidomain\Models\Token;
use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateItcomMultidomainTokens extends Migration
{
    public function up()
    {
        Schema::create(Token::TABLE, function ($table)
        {
            Schema::dropIfExists(Token::TABLE);
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 125);
            $table->text('value');
            $table->integer('domain_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Token::TABLE);
    }
}
