<?php return [
    'plugin'    => [
        'name'        => 'Multidomain',
        'description' => 'Разделение контента по поддоменам',
    ],
    'common'    => [
        'domains'              => 'Домены',
        'tokens'               => 'Токены',
        'info'                 => 'Справка',
        'setting_multidomains' => 'Настройка мультидоменов',
        'saving'               => 'Сохранение...',
        'sure_delete'          => 'Удалить выбранную строку?',
    ],
    'actions'   => [
        'new_domain'     => 'Новый домен',
        'delete_domain'  => 'Удалить домен',
        'new_token'      => 'Новый токен',
        'delete_token'   => 'Удалить токен',
        'create'         => 'Создать',
        'create_close'   => 'Создать и закрыть',
        'cancel'         => 'Отмена',
        'return_to_list' => 'Вернуться к списку',
        'save'           => 'Сохранить',
        'save_close'     => 'Сохранить и закрыть',
    ],
    'component' => [
        'city' => [
            'name'        => 'Выбор города',
            'description' => 'Позволяет выбрать текущий город в шапке сайта',
        ],
    ],
];