<?php namespace Itcom\Multidomain\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Info Back-end Controller
 */
class Info extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'Справка';

        BackendMenu::setContext('Itcom.Multidomain', 'multidomain', 'info');
    }

    /**
     * @return void
     */
    public function index()
    {
    }

}
