<?php namespace Itcom\Multidomain\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Domains Back-end Controller
 */
class Domains extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['itcom.multidomain.setting_multidomains'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Itcom.Multidomain', 'multidomain', 'domains');
    }
}
