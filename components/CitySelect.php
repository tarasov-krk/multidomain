<?php namespace Itcom\Multidomain\Components;

use Request;
use Cms\Classes\ComponentBase;
use Itcom\Multidomain\Classes\DomainService;

/**
 * Class CitySelect
 *
 * @package Itcom\Multidomain\Components
 */
class CitySelect extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'itcom.multidomain::lang.component.city.name',
            'description' => 'itcom.multidomain::lang.component.city.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @return void
     */
    public function onRun()
    {
        $this->page['cityList'] = DomainService::getListDomains();
    }

    /**
     * @return string
     */
    public function getCurrentDomainName()
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        $result = 'Москва';
        if ($domainData = DomainService::getCurrentDomainData()) {
            $result = $domainData->name;
        }

        return $result;

        // </editor-fold>
    }

    /**
     * @author a.tarasov
     * @access public
     * @return string
     */
    public function getUri()
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        return Request::getRequestUri();

        // </editor-fold>
    }

}
